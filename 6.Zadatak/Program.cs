﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            GroupNote group1 = new GroupNote("List of people",  new RedTheme());
            group1.addPeople("Marko");
            group1.addPeople("David");
            GroupNote group2 = new GroupNote("New list", new LightTheme());
            group2.addPeople("Josip");
            group2.addPeople("Lara");

            group1.Show();
            group2.Show();
        }
    }
}
