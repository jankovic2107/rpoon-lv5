﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Zadatak
{
    class ShippingService
    {
        private double unitPrice;
        public ShippingService(double unitPrice)
        {
            this.unitPrice = unitPrice;
        }
        public double calculateDeliveryPrice(IShipable unit)
        {
            double deliveryPrice = unit.Weight * this.unitPrice;
            return deliveryPrice;
        }
    }
}
