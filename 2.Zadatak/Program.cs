﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            ShippingService shippingService = new ShippingService(4);
            Box box = new Box("First box");
            Product product1 = new Product("Fragile", 200, 40);
            Product product2 = new Product("Music", 100, 30);
            box.Add(product1);
            box.Add(product2);
            Console.WriteLine(box.Description(1));
            Console.WriteLine(shippingService.calculateDeliveryPrice(box));
        }
    }
}
