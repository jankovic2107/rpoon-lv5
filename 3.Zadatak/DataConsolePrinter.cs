﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.Zadatak
{
    class DataConsolePrinter
    {
        public DataConsolePrinter() { }
        public void showData(IDataset dataset)
        {
            if (dataset.GetData() != null)
            {
                foreach (List<string> list in dataset.GetData())
                {
                    foreach (string item in list)
                    {
                        Console.Write(item + " ");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
        }
    }
}
