﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset(@"C:\Users\Ivan Jankovic\Desktop\RPOON-LV5.txt");
            User user1 = User.GenerateUser("Marko");
            User user2 = User.GenerateUser("David");
            User user3 = User.GenerateUser("Josip");
            ProtectionProxyDataset protectionProxyDataset1 = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset protectionProxyDataset2 = new ProtectionProxyDataset(user2);
            ProtectionProxyDataset protectionProxyDataset3 = new ProtectionProxyDataset(user3);
            DataConsolePrinter dataConsolePrinter = new DataConsolePrinter();

            dataConsolePrinter.showData(virtualProxyDataset);
            dataConsolePrinter.showData(protectionProxyDataset1);
            dataConsolePrinter.showData(protectionProxyDataset2);
            dataConsolePrinter.showData(protectionProxyDataset3);

        }
    }
}
