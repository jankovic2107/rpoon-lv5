﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.Zadatak
{
    class GroupNote : Note
    {
        private List<String> groupOfPeople;

        public GroupNote(String message, ITheme theme) : base(message, theme)
        {
            groupOfPeople = new List<String>();
        }

        public void addPeople(String name)
        {
            groupOfPeople.Add(name);
        }

        public void removePeople(String name)
        {
            foreach(String person in groupOfPeople)
            {
                if(person == name)
                {
                    groupOfPeople.Remove(person);
                }
            }
        }
        public String ConvertPeopleToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach(String person in this.groupOfPeople)
            {
                stringBuilder.Append(person + '\n');
            }
            return stringBuilder.ToString();
        }

        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("People: ");
            String framedMessage = this.GetFramedMessage();
            String framedGroup = this.ConvertPeopleToString();
            Console.WriteLine(framedMessage);
            Console.WriteLine(ConvertPeopleToString());
        }

    }
}
