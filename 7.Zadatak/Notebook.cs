﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.Zadatak
{
    class Notebook
    {
        private List<Note> notes;
        private ITheme theme;
        public Notebook() {
            this.notes = new List<Note>();
        }
        public Notebook(ITheme theme):this()
        {
            this.theme = theme;
        }

        public void AddNote(Note note) { this.notes.Add(note); }
        public void ChangeTheme(ITheme theme)
        {
            foreach (Note note in this.notes)
            {
                note.Theme = theme;
            }
        }
        public void Display()
        {
            foreach (Note note in notes)
            {
                if (this.theme != null)
                {
                    note.Theme = theme;
                }
                note.Show();
                Console.WriteLine("\n");
             
            }
        }
    }
}
