﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme theme1 = new LightTheme();
            RedTheme theme2 = new RedTheme();

            
            GroupNote group1 = new GroupNote("List of people", new RedTheme());
            group1.addPeople("Marko");
            group1.addPeople("David");
            GroupNote group2 = new GroupNote("New list", new LightTheme());
            group2.addPeople("Josip");
            group2.addPeople("Lara");
            RedTheme redTheme = new RedTheme();
            ReminderNote reminderNote = new ReminderNote("This is my message", redTheme);


            Notebook notebook = new Notebook();
            notebook.AddNote(group1);
            notebook.AddNote(reminderNote);
            notebook.Display();

            Console.WriteLine();
            Notebook secondNotebook = new Notebook(theme1);
            secondNotebook.AddNote(group1);
            secondNotebook.AddNote(reminderNote);
            secondNotebook.Display();
        }
    }
}
