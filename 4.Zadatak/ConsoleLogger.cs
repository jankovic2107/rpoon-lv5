﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.Zadatak
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;
        private ConsoleLogger() { }

        public static ConsoleLogger GetInstance()
        {
            if (instance == null)
                instance = new ConsoleLogger();
            return instance;
        }

        public void LogIn()
        {
            Console.WriteLine("Dataset file was accessed at: " + DateTime.Now);
        }
    }
}