﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.Zadatak
{
    class LoggerProxy : IDataset
    {
        private String filePath;
        private ConsoleLogger consoleLogger;
        private Dataset dataset;

        public LoggerProxy(String filePath)
        {
            this.filePath = filePath;
            consoleLogger = ConsoleLogger.GetInstance();
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            if(this.dataset != null)
            {
                consoleLogger.LogIn();
            }
            if (this.dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            return dataset.GetData();
        }
    }
}
