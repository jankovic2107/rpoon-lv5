﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.Zadatak
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}
