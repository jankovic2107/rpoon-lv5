﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            LoggerProxy loggerProxy = new LoggerProxy(@"C:\Users\Ivan Jankovic\Desktop\RPOON-LV5.txt");
            DataConsolePrinter dataConsolePrinter = new DataConsolePrinter();
            dataConsolePrinter.showData(loggerProxy);
        }
    }
}
