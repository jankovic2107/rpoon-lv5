﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            RedTheme redTheme = new RedTheme();
            ReminderNote reminderNote = new ReminderNote("This is my message", redTheme);
            reminderNote.Show();
        }
    }
}
